#! /bin/sh
set -e

usage () {
    cat >&2 <<EOF
usage: $0 <image-kind>

EOF
}

say () {
    printf "[Micodkr] " >&2
    printf "$@" >&2
    printf "\n" >&2
}

if ! [ -f src/scripts/make-docker-images.sh ] ; then
    say "This script should run from the root of the tree."
    usage
    exit 1
fi

not_empty () {
    if [ "$1" = "" ] ; then
        say "$2 should not be empty!"
        usage
        exit 2
    fi
}

make_full_dockerfile () {
    cat > Dockerfile <<EOF
FROM ocaml/opam2:alpine-3.10-ocaml-4.08
WORKDIR /home/opam/opam-repository
RUN git pull
WORKDIR /
RUN opam update
RUN sudo mkdir -p /build
RUN sudo chown -R opam:opam /build
WORKDIR /build
ADD --chown=opam:opam . ./
RUN make vendors
RUN opam exec -- make deps
RUN opam exec -- make
EOF
}

make_run_dockerfile () {
    from_image="$1"
    not_empty "$from_image" "The from-image"
    cat > Dockerfile <<EOF
FROM $from_image
FROM alpine
RUN apk update
RUN apk add curl gmp-dev libev-dev
COPY --from=0 /build/_build/default/src/app/main.exe /usr/bin/micodef
RUN chmod a+rx /usr/bin/micodef
EOF
}

image_kind="$1"
not_empty "$image_kind" "Argument <image-kind>"

case "$image_kind" in
    "build" )
        make_full_dockerfile
        actual_tag="$docker_tag-build"
        ;;
    "run" )
        shift
        make_run_dockerfile "$1"
        actual_tag="$docker_tag-run"
        ;;
    * )
        say "Error unknown image-kind"
        usage
        exit 2
        ;;
esac
say "Dockerfile: "
cat Dockerfile


