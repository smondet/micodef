#! /bin/sh
set -e

usage () {
    cat >&2 <<EOF
usage: $0 <output-path>

EOF
}

say () {
    printf "[Micodoc] " >&2
    printf "$@" >&2
    printf "\n" >&2
}

if ! [ -f src/scripts/make-doc.sh ] ; then
    say "This script should run from the root of the tree."
    usage
    exit 1
fi

output_path="$1"

mkdir -p "$output_path"

example_names="$(./micodef examples --name --all)"
examples_dir=./examples/
example_markdown_list=""
mkdir -p "$output_path/$examples_dir"
for ex in $example_names ; do
    json="$examples_dir/$ex.json"
    ./micodef examples "$ex" -o "$output_path/$json"
    exj="$output_path/$json"
    yaml="$examples_dir/$ex-yaml.txt"
    ./micodef yaml --spec "$exj" -o "$output_path/$yaml"
    html="$examples_dir/$ex.html"
    ./micodef html --spec "$exj" -o "$output_path/$html"
    html_s="$examples_dir/$ex-simple.html"
    ./micodef html --spec "$exj" --simpl -o "$output_path/$html_s"
    blob=$(./micodef examples --desc "$ex" | grep "$ex" | sed 's/.$/: /')
    example_markdown_list="$example_markdown_list
$blob [JSON]($json), [YAML]($yaml), [HTML]($html), [HTML-simple]($html_s)."
done



./micodef json-schema -o "$output_path/schema.json"

opam config exec -- opam install --yes odig omd odoc
opam config exec -- dune build @src/lib_specification/doc
opam config exec -- dune build @src/lib_html/doc

cp -r _build/default/_doc/_html/* "$output_path/"

cp -r $(odig odoc-theme path odig.light)/* "$output_path/"

make_page () {
    input="$1"
    output="$2"
    title="$3"
cat > "$output" <<EOF
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>$title</title>
    <link rel="stylesheet" href="./odoc.css"/>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
  </head>
  <body>
    <main class="content">
EOF
cat "$input" >> "$output"
cat >> "$output" <<'EOF'
    </main>
  </body>
</html>
EOF
}


yaml_example () {
    name="$1"
    md="$2"
    yaml=examples/$name/specification.yaml
    html=examples/$name.html
    json=examples/$name.json
    yaml_txt=examples/$name-yaml.txt
    ./micodef html --spec $yaml -o $output_path/$html
    ./micodef json --spec $yaml -o $output_path/$json
    cp $yaml $output_path/$yaml_txt
    echo "👉 Source [YAML]($yaml_txt) → Generated " >> "$md"
    echo "[HTML]($html) and" >> "$md"
    echo "[JSON]($json)." >> "$md"
}

artwork_licensing_md=$(mktemp "/tmp/al-XXXX.md")
yaml_example artwork-licensing "$artwork_licensing_md"
tzip7_md=$(mktemp "/tmp/al-XXXX.md")
yaml_example fa-one-dot-something "$tzip7_md"

main_index_fragment=$(mktemp "/tmp/main-index-XXXX.html")
{
    cat README.md
    cat <<EOF
## Generated Schema

The definition of the specification format: [\`schema.json\`](./schema.json).

## Examples

EOF
    cat examples/README.md | \
        sed 's/^# .*$//' | \
        sed 's/^## /###/' | \
        awk "1 ; /<!--artwork-licensing-->/ { system(\"cat $artwork_licensing_md\") }" | \
        awk "1 ; /<!--fa-one-dot-something-->/ { system(\"cat $tzip7_md\") }" | \
        cat
    cat <<EOF

### Built-in Examples

Meaningless examples provided by the \`micodef\` application converted
to various formats:

$example_markdown_list

## OCaml API Documentation

Check-out some entry-point modules:

- In the library \`micodef-specification\`: [\`Micodef_specification.V0\`](./micodef-specification/Micodef_specification/V0/index.html)
- In the library \`micodef-html\`: [\`Micodef_html.Export\`](./micodef-html/Micodef_html/Export/index.html)

EOF
} | omd >> "$main_index_fragment"

make_page "$main_index_fragment" "$output_path/index.html" "Micodef: WIP"


say "Done: file://$output_path"





