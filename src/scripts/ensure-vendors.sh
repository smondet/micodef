#! /bin/sh

set -e

usage () {
    cat >&2 <<EOF
usage: $0

This script is meant to be called by the toplevel Makefile:

    make vendors
EOF
}

say () {
    printf "[Ensure_vendors] " >&2
    printf "$@" >&2
    printf "\n" >&2
}

if ! [ -f src/scripts/ensure-vendors.sh ] ; then
    say "This script should run from the root of the tree."
    usage
    exit 1
fi

ensure_vendor () {
    name="$1"
    remote="$2"
    branch="$3"
    witness="$4"
    if [ -f "local-vendor/$name/$witness" ] ; then
        say "'$name' already cloned"
    else
        mkdir -p local-vendor/
        git clone --depth 100 "$remote" -b "$branch"  local-vendor/$name
    fi
    (
        cd "local-vendor/$name"
        git checkout "$branch"
        git pull
        git log --oneline -n 5
    )
}

# We used to get https://gitlab.com/nomadic-labs/data-encoding/merge_requests/6
ensure_vendor json-data-encoding \
              https://gitlab.com/nomadic-labs/json-data-encoding \
              master \
              README.md
ensure_vendor data-encoding \
              https://gitlab.com/nomadic-labs/data-encoding \
              master \
              README.md

