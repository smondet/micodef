open Base

type options =
  { section_base: int
  ; simplistic: bool
  ; css_uris: string list
  ; inline_css: string option
  ; get_external: string -> string }

let default_inline_css =
  {css|
body {
  font-family: sans;
}
section {
  margin-left: 20px;
}
section h2,h3,h4,h5 {
  margin-left: -20px;
}
.error {
  border: solid 2px red;
  padding: 5px;
}
table.spec-status {
  border-collapse: collapse;
}
.spec-status table, th, td {
  border: 1px solid black;
  text-align: left;
  vertical-align: top;
  padding: 1px 3px 1px 3px;
}
.michelson-pre {
  padding: 0px;
  margin: 2px 2px 2px 20px;
  border-left: solid 1px #080;
}
.michelson-type {
  margin: 0px;
  font-weight: bold;
  color: #411;
}
.michelson-annotation {
  font-weight: bold;
  color: #141;
}
|css}

let defaults =
  { section_base= 2
  ; css_uris= []
  ; simplistic= false
  ; inline_css= Some default_inline_css
  ; get_external=
      Caml.(
        fun s ->
          let i = open_in s in
          let b = Buffer.create 42 in
          let rec loop () =
            try
              Buffer.add_char b (input_char i) ;
              loop ()
            with _ -> () in
          loop () ; Buffer.contents b) }

let omd_to_html ?with_par blob =
  match
    (with_par, String.find blob ~f:(function '\n' -> true | _ -> false))
  with
  | None, None | Some false, _ ->
      let md = Omd.of_string blob |> Omd.to_html in
      assert (String.is_prefix md ~prefix:"<p>") ;
      assert (String.is_suffix md ~suffix:"</p>\n") ;
      String.sub md ~pos:3 ~len:(String.length md - 3 - 5)
  | Some true, _ | _, Some _ -> Omd.of_string blob |> Omd.to_html

let omd_to_tyxml blob =
  let data = omd_to_html blob in
  Tyxml.Html.Unsafe.data data

let specification ?(options = defaults) spec =
  let open Micodef_specification.V0 in
  let open Tyxml.Html in
  let h n =
    match n + options.section_base - 1 with
    | 1 -> h1
    | 2 -> h2
    | 3 -> h3
    | 4 -> h4
    | 5 -> h5
    | 6 -> h6
    | _ -> assert false in
  let txtf fmt = Fmt.kstr txt fmt in
  let codef fmt = Fmt.kstr (fun c -> code [txt c]) fmt in
  let version_string =
    match spec.status with
    | `Version s -> "V. " ^ s
    | `Work_in_progress -> "WIP" in
  let error_block content = div ~a:[a_class ["error"]] content in
  let prose_bloc =
    let markdown s = omd_to_tyxml s in
    function
    | Prose.Inline_markdown mkdn -> markdown mkdn
    | Prose.External e when options.simplistic ->
        p [txt "{"; a ~a:[a_href e] [txt e]; txt "}"]
    | Prose.External path -> (
      try
        let md = options.get_external path in
        markdown md
      with e ->
        error_block
          [ txt "Error while getting markdown file: "
          ; code [txt path]
          ; txt " → "
          ; code [txt (Exn.to_string e)] ] ) in
  let description_bloc = Option.map spec.introduction ~f:prose_bloc in
  let status_table =
    match options.simplistic with
    | false ->
        let version = tr [th [txtf "Status"]; td [txt version_string]] in
        let authors =
          match spec.authors with
          | [] -> [tr [td ~a:[a_colspan 2] [i [txtf "No authors defined"]]]]
          | one :: more ->
              tr
                [ th ~a:[a_rowspan (List.length more + 1)] [txtf "Authors"]
                ; td [codef "%s" one] ]
              :: List.map more ~f:(fun auth -> tr [td [codef "%s" auth]]) in
        table (version :: authors) ~a:[a_class ["spec-status"]]
    | true ->
        ul
          (List.map ~f:li
             [ [b [txtf "Status: %s" version_string]]
             ; [ b [txtf "Authors:"]
               ; ( match spec.authors with
                 | [] -> codef "NONE"
                 | more ->
                     ul (List.map ~f:(fun auth -> li [codef "%s" auth]) more)
                 ) ] ]) in
  let michelson_type t =
    match t with
    | Michelson_type.Raw s when String.mem s '\n' ->
        Fmt.epr "RAU: %S\n%!" s ;
        pre
          ~a:[a_class ["michelson-pre"]]
          [code ~a:[a_class ["michelson-type"]] [txt (String.strip s)]]
    | Michelson_type.Raw s -> code ~a:[a_class ["michelson-type"]] [txt s]
  in
  let documented_michelson Documented_michelson.{michelson; annotations} =
    let Michelson_type_with_holes.{hole_primitives; expression} = michelson in
    let holes =
      List.map hole_primitives ~f:(fun {placeholder; description} ->
          (placeholder, description)) in
    let annots = List.map annotations ~f:(fun (k, p) -> (k, p)) in
    let details =
      match holes @ annots with
      | [] -> []
      | more ->
          [ ul
              (List.map more ~f:(fun (annot, desc) ->
                   li
                     ( [code ~a:[a_class ["michelson-annotation"]] [txt annot]]
                     @ [txt ": "; prose_bloc desc] ))) ] in
    div ([div [txt "Type: "; michelson_type expression]] @ details) in
  let entry_points ~level = function
    | [] -> []
    | more ->
        List.concat_map more
          ~f:
            Entry_point.(
              fun ep ->
                [ section
                    [ h level [txtf "Entry-point "; codef "%s" ep.name]
                    ; documented_michelson ep.signature
                    ; Option.value_map ~f:prose_bloc ep.description
                        ~default:(p [b [txt "NO DESCRIPTION"]]) ] ]) in
  let contracts ~level =
    match spec.contracts with
    | [] ->
        [blockquote [i [txt "This specification does not define any contract"]]]
    | _ ->
        List.map spec.contracts ~f:(fun contract ->
            section
              ( [ h ~a:[a_id contract.name] level
                    [txt "Contract "; codef "'%s'" contract.name] ]
              @ Option.value_map contract.description ~default:[] ~f:(fun d ->
                    [prose_bloc d])
              @ Option.value_map contract.storage_type ~default:[] ~f:(fun m ->
                    [ section
                        [ h (level + 1) [txt "Storage Type"]
                        ; documented_michelson m ] ])
              @ entry_points ~level:(level + 1) contract.entry_points )) in
  let document =
    [h 1 [codef "'%s'" spec.name; txtf ": %s" spec.title]; div [status_table]]
    @ Option.value_map ~default:[] description_bloc ~f:(fun b ->
          [section [h 2 [txtf "Description"]; b]])
    @ contracts ~level:2 in
  let head_stuff =
    [meta ~a:[a_charset "utf-8"] ()]
    @ List.concat_map options.css_uris ~f:(fun href ->
          [link ~rel:[`Stylesheet] ~href ()]) in
  let inline_css =
    Option.value_map ~default:[] options.inline_css ~f:(fun css ->
        [style [txt css]]) in
  html
    (head (title (txtf "%s %s" spec.name version_string)) head_stuff)
    (body (inline_css @ document))
