open Base

module Specification_v0 = struct
  open Micodef_specification.V0

  module Example = struct
    let zero = make "empty-spec" "The minimal specification"

    let one =
      make "one-entry-point"
        "Example specification with one single entry-point"
        ~contracts:
          [ Contract.make "main"
              ~entry_points:
                [ Entry_point.make "hello"
                    (Michelson_type_with_holes.raw "pair nat signature") ] ]

    let two =
      make "two-entry-point" "Example specification with two entry-points"
        ~contracts:
          [ Contract.make "main"
              ~entry_points:
                [ Entry_point.make "hello"
                    (Michelson_type_with_holes.raw
                       "pair (nat %newvalue) (signature %authorization)")
                ; Entry_point.make "world"
                    (Michelson_type_with_holes.raw "pair key (pair nat mutez)")
                    ~description:
                      Prose.(
                        inline_markdown
                          "This is the **second entry-point** apparently.") ]
          ]

    let forty =
      let main_contract =
        Contract.make "main"
          ~description:
            Prose.(
              inline_markdown
                "The `main` contract, it will call the [`token`](#token) one.")
          ~storage_type:
            Michelson_type_with_holes.(
              raw
                ~annotations:
                  [ ( "%counter"
                    , Prose.inline_markdown
                        "The counter to use while signing is available here \
                         for RPC use." ) ]
                ~placeholders:
                  [ placeholder
                      ~description:
                        (Prose.inline_markdown
                           "Part of the type left to the implementation.")
                      "_implementation_specific_" ]
                "(pair (nat %counter) _implementation_specific_)")
          ~entry_points:
            [ Entry_point.make "hello"
                ~description:
                  Prose.(
                    inline_markdown
                      "The first entry-point just says *“hello”* to the \
                       contract.")
                (Michelson_type_with_holes.raw "pair nat signature")
            ; Entry_point.make "world"
                (Michelson_type_with_holes.raw
                   "pair (key %user) (pair (nat %number) (mutez %price))"
                   ~annotations:
                     Prose.
                       [ ( "%user"
                         , inline_markdown
                             "The key of the user requesting something." )
                       ; ( "%price"
                         , inline_markdown "The amount of tez something costs."
                         ) ])
                ~description:
                  Prose.(
                    inline_markdown
                      "This is the **second entry-point** apparently.") ] in
      let token =
        Contract.make "token"
          ~description:
            Prose.(
              inline_markdown "The non-[`main`](#main) contract, obviously.")
          ~entry_points:
            [ Entry_point.make "buy"
                ~description:Prose.(inline_markdown "Buy **something**.")
                (Michelson_type_with_holes.raw "pair nat signature") ] in
      make "full-example" "Fuller specification with many fields filled"
        ~introduction:Prose.(uri "file://./full-example-intro-blob.md")
        ~status:(`Version "0.0.1+dev")
        ~authors:["Alice <alice@example.com>"; "Bob <bob@example.org>"]
        ~contracts:[main_contract; token]

    let all = [zero; one; two; forty]
  end

  let pp_quick ppf {name; _} =
    let open Fmt in
    pf ppf "%s" name
end

let () =
  let open Cmdliner in
  let help = Term.(ret (pure (`Help (`Auto, None))), info "micodef") in
  let output_file_term () =
    let open Term in
    Arg.(
      pure (fun p -> `Output_path p)
      $ value
          (opt (some string) None
             (info ["output"; "o"] ~doc:"Output to a file."))) in
  let with_output (`Output_path path_opt) f =
    let outchan =
      Option.value_map path_opt ~default:Caml.stdout ~f:Caml.open_out in
    let ppf = Caml.Format.formatter_of_out_channel outchan in
    let res = f ppf in
    Fmt.flush ppf () ;
    Option.iter path_opt ~f:(fun p ->
        Caml.close_out outchan ;
        Fmt.epr "See file://%s\n%!"
          ( if Caml.Filename.is_relative p then
            Caml.Filename.concat (Caml.Sys.getcwd ()) p
          else p )) ;
    res in
  let input_spec_term () =
    let open Term in
    Arg.(
      pure (fun force_yaml path ->
          let actual_json =
            match force_yaml || Caml.Filename.check_suffix path ".yaml" with
            | true ->
                Fmt.epr "File '%s' will be parsed as YAML.\n%!" path ;
                let i = Caml.open_in path in
                let b = Buffer.create 42 in
                let rec loop () =
                  try
                    Buffer.add_char b (Caml.input_char i) ;
                    loop ()
                  with _ -> () in
                loop () ;
                Caml.close_in i ;
                Yaml.of_string_exn (Buffer.contents b)
            | false ->
                let i = Caml.open_in path in
                let json = Ezjsonm.value_from_channel i in
                Caml.close_in i ; json in
          Micodef_specification.V0.Codec.of_json actual_json)
      $ value (flag (info ["yaml"] ~doc:"Parse specification as YAML."))
      $ required
          (opt (some string) None
             (info ["specification"]
                ~doc:"Load a specification (JSON or YAML file)."))) in
  let examples_cmd =
    let open Term in
    let result_term =
      ret
        ( pure (fun just_describe just_names all examples output ->
              let examples =
                if all then Specification_v0.Example.all else examples in
              with_output output (fun ppf ->
                  match (just_names, just_describe) with
                  | true, true ->
                      `Error (false, "Cannot use `--describe` with `--names`.")
                  | true, false ->
                      let open Fmt in
                      List.iter examples ~f:(fun ex -> pf ppf "%s\n%!" ex.name) ;
                      `Ok ()
                  | false, true ->
                      let open Fmt in
                      vbox ~indent:2
                        (fun ppf () ->
                          pf ppf "Examples:" ;
                          List.iter examples ~f:(fun ex ->
                              pf ppf "@,* `%s`: %s." ex.name ex.title))
                        ppf () ;
                      pf ppf "\n%!" ;
                      `Ok ()
                  | false, false ->
                      let json =
                        match examples with
                        | [one] -> Micodef_specification.V0.Codec.to_json one
                        | _ ->
                            Ezjsonm.list Micodef_specification.V0.Codec.to_json
                              examples in
                      Fmt.pf ppf "%s\n%!"
                        (Ezjsonm.value_to_string ~minify:false json) ;
                      `Ok ()))
        $ Arg.(
            value
              (flag
                 (info ["describe"] ~doc:"Just describe the chosen examples.")))
        $ Arg.(
            value
              (flag
                 (info ["name"]
                    ~doc:
                      "Just list the names of the chosen examples \
                       (script-friendly).")))
        $ Arg.(
            value
              (flag (info ["all"] ~doc:"Use all of the available examples.")))
        $ Arg.(
            value
              (pos_all
                 (enum
                    (List.map Specification_v0.Example.all ~f:(fun ex ->
                         (ex.name, ex))))
                 []
                 (info [] ~docv:"EXAMPLE-NAMES"
                    ~doc:"Names of the examples to treat.")))
        $ output_file_term () ) in
    let meta_info =
      info "examples" ~doc:"Use a bunch of examples."
        ~man:
          ( [ `S "ARGUMENTS"; `S "AVAILABLE EXAMPLES"
            ; `P "Here are the available specifications:" ]
          @ List.map Specification_v0.Example.all ~f:(fun ex ->
                `I (ex.name, ex.title))
          @ [`S "OPTIONS"] ) in
    (result_term, meta_info) in
  let schema_cmd =
    let open Term in
    ( pure (fun output ->
          with_output output (fun ppf ->
              Fmt.pf ppf "%s\n%!"
                (Ezjsonm.value_to_string ~minify:false
                   Micodef_specification.V0.Codec.json_schema)))
      $ output_file_term ()
    , info "json-schema" ~doc:"Output the JSON schema" ) in
  let html_cmd =
    let open Term in
    ( pure (fun css_uris simplistic output spec ->
          let module H = Micodef_html.Export in
          let options = H.{defaults with css_uris; simplistic} in
          let tyhtml = H.specification ~options spec in
          with_output output (fun ppf ->
              Fmt.pf ppf "%a\n%!" (Tyxml.Html.pp ~indent:true ()) tyhtml))
      $ Arg.(value (opt_all string [] (info ["css"] ~doc:"Add link to CSS.")))
      $ Arg.(
          value (flag (info ["simplistic"] ~doc:"Generate simplistic HTML.")))
      $ output_file_term () $ input_spec_term ()
    , info "html" ~doc:"Output as basic HTML" ) in
  let yaml_cmd =
    let open Term in
    let term =
      pure (fun output input_spec ->
          with_output output (fun ppf ->
              let open Result in
              let json_value =
                Micodef_specification.V0.Codec.to_json input_spec in
              ( match
                  Yaml.of_json json_value
                  >>= fun yaml ->
                  Yaml.yaml_to_string ~encoding:`Utf8 ~layout_style:`Block yaml
                with
              | Ok yaml -> Fmt.string ppf yaml
              | Error (`Msg s) ->
                  Fmt.epr "Yaml-error: %s\n%!" s ;
                  failwith "YAML conversion failed." ) ;
              Fmt.epr "Done\n%!"))
      $ output_file_term () $ input_spec_term () in
    (term, info "yaml" ~doc:"Convert specification to “canonical” YAML.")
  in
  let json_cmd =
    let open Term in
    let term =
      pure (fun output input_spec ->
          with_output output (fun ppf ->
              let json_value =
                Micodef_specification.V0.Codec.to_json input_spec in
              Fmt.string ppf (Ezjsonm.value_to_string ~minify:false json_value) ;
              Fmt.cut ppf () ;
              Fmt.epr "Done\n%!"))
      $ output_file_term () $ input_spec_term () in
    (term, info "json" ~doc:"Convert specification to “canonical” JSON.")
  in
  Term.exit
    (Term.eval_choice
       (help : unit Term.t * _)
       [examples_cmd; schema_cmd; html_cmd; yaml_cmd; json_cmd])
