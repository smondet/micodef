open Base

module Prose = struct
  type t = Inline_markdown of string | External of string
  [@@deriving compare, equal, sexp]

  let inline_markdown s = Inline_markdown s
  let uri u = External u
end

module Documented_michelson = struct
  type 'a t = {michelson: 'a; annotations: (string * Prose.t) list}
  [@@deriving compare, equal, sexp]

  let make annotations michelson = {annotations; michelson}
end

module Michelson_type = struct
  type t = Raw of string [@@deriving compare, equal, sexp]

  let make_raw s = Raw s
  let raw ?(annotations = []) s = Documented_michelson.make annotations (Raw s)
  let to_michelson_string = function Raw s -> s
end

module Michelson_type_with_holes = struct
  type hole = {placeholder: string; description: Prose.t}
  [@@deriving compare, equal, sexp]

  type t = {hole_primitives: hole list; expression: Michelson_type.t}
  [@@deriving compare, equal, sexp]

  let placeholder ~description placeholder = {placeholder; description}

  let make ?(annotations = []) ?(hole_primitives = []) expression =
    Documented_michelson.make annotations {hole_primitives; expression}

  let raw ?annotations ?placeholders s =
    make ?annotations ?hole_primitives:placeholders (Michelson_type.make_raw s)
end

module Entry_point = struct
  type t =
    { name: string
    ; signature: Michelson_type_with_holes.t Documented_michelson.t
    ; description: Prose.t option }
  [@@deriving compare, equal, sexp]

  let make ?description name signature = {name; signature; description}
end

module Contract = struct
  type t =
    { name: string
    ; description: Prose.t option
    ; entry_points: Entry_point.t list
    ; storage_type: Michelson_type_with_holes.t Documented_michelson.t option
    }
  [@@deriving compare, equal, sexp]

  let make ?description ?(entry_points = []) ?storage_type name =
    {name; description; entry_points; storage_type}
end

type t =
  { name: string
  ; title: string
  ; introduction: Prose.t option
  ; authors: string list
  ; status: [`Work_in_progress | `Version of string]
  ; contracts: Contract.t list }
[@@deriving compare, equal, sexp]

let make ?(status = `Work_in_progress) ?(authors = []) ?(contracts = [])
    ?introduction name title =
  {name; status; authors; contracts; title; introduction}

module Codec = struct
  let encoding =
    let name = "contract-specification" in
    let open Data_encoding in
    let def t = def (Fmt.str "%s.%s" name t) in
    let michelson_type =
      let open Michelson_type in
      def "michelson-type" ~title:"Michelson type expressions"
        ~description:
          "Definition of Michelson types (for entry-point parameters)."
        (union
           [ case ~title:"raw-michelson" (Tag 0)
               ~description:
                 "Definition of Michelson types using Michelson's concrete \
                  syntax."
               (obj1 (req "raw-michelson" string))
               (function Raw r -> Some r)
               (fun r -> Raw r)
           ; case ~title:"default-raw-michelson" (Tag 1)
               ~description:
                 "Definition of Michelson types using Michelson's concrete \
                  syntax."
               string
               (function Raw r -> Some r)
               (fun r -> Raw r) ]) in
    let prose =
      let open Prose in
      def "prose" ~title:"Human language description"
        ~description:
          "Human-language-based description (usually English in Markdown)."
        (union
           [ case ~title:"inline-markdown" (Tag 0)
               ~description:"Markdown prose as a string."
               (obj1 (req "inline-markdown" string))
               (function Inline_markdown m -> Some m | _ -> None)
               (fun m -> Inline_markdown m)
           ; case ~title:"external" (Tag 1)
               ~description:
                 "URI referencing Markdown prose in another document."
               (obj1 (req "external" string))
               (function External u -> Some u | _ -> None)
               (fun u -> External u)
           ; case ~title:"default-inline-markdown" (Tag 2)
               ~description:"Markdown prose as a string." string
               (function Inline_markdown m -> Some m | _ -> None)
               (fun m -> Inline_markdown m) ]) in
    let michelson_type_with_holes =
      let open Michelson_type_with_holes in
      let placeholders =
        conv
          (List.map ~f:(fun {placeholder; description} ->
               (placeholder, description)))
          (List.map ~f:(fun (placeholder, description) ->
               {placeholder; description}))
          (assoc prose) in
      def "michelson-type-with-holes"
        ~title:"Michelson type expression with unknowns"
        ~description:
          "Michelson type expressions with missing/unknown sub-expressions \
           (using placeholders)."
        (conv
           (fun {hole_primitives; expression} -> (hole_primitives, expression))
           (fun (hole_primitives, expression) -> {hole_primitives; expression})
           (obj2
              (dft "placeholders" placeholders []
                 ~title:"Hole/Placeholder list")
              (req "expression" michelson_type))) in
    let annotations = obj1 (dft "annotations" (assoc prose) []) in
    let documented michelson =
      let open Documented_michelson in
      conv
        (fun {michelson; annotations} -> (michelson, annotations))
        (fun (michelson, annotations) -> {michelson; annotations})
        (merge_objs michelson annotations) in
    let entry_point =
      let open Entry_point in
      def "entry-point" ~title:"Specification of a contract entry-point"
        ~description:
          "Description and type definition of a contract entry-point. Types \
           of entry-points for which operations are only meant to be queried \
           may have partial types."
        (conv
           (fun {name; signature; description} ->
             (name, signature, description))
           (fun (name, signature, description) ->
             {name; signature; description})
           (obj3
              (req "name" string ~title:"Michelson name of the entry-point")
              (req "type"
                 (documented michelson_type_with_holes)
                 ~title:"Partial type"
                 ~description:"Partial type/signature of the entry-point.")
              (opt "description" prose ~title:"Description"))) in
    let status =
      union
        [ case ~title:"Work-in-progress" (Tag 0)
            ~description:
              "The string `work-in-progress` means that no version has been \
               set yet."
            (constant "work-in-progress")
            (function `Work_in_progress -> Some () | _ -> None)
            (fun () -> `Work_in_progress)
        ; case ~title:"Version-status" (Tag 1)
            ~description:"The current version of the specification."
            (obj1 (req "version" string ~title:"Version string"))
            (function `Version s -> Some s | _ -> None)
            (fun s -> `Version s) ] in
    let contract =
      let open Contract in
      conv
        (fun {name; description; entry_points; storage_type} ->
          (name, description, entry_points, storage_type))
        (fun (name, description, entry_points, storage_type) ->
          {name; description; entry_points; storage_type})
        (obj4
           (req "name" string ~title:"Short name of the contract"
              ~description:"Filename-friendly name for the contract interface.")
           (opt "description" prose ~title:"Description")
           (dft "entry-points" (list entry_point) [] ~title:"Entry points")
           (opt "storage-type"
              (documented michelson_type_with_holes)
              ~title:"Partial storage type")) in
    let v0 =
      def "specification"
        ~title:"Specification of a set of contracts interfaces"
        ~description:"Top-level definition of a set of contract interfaces."
        (conv
           (fun {name; status; authors; contracts; title; introduction} ->
             (name, title, status, authors, introduction, contracts))
           (fun (name, title, status, authors, introduction, contracts) ->
             {name; status; authors; title; introduction; contracts})
           (obj6
              (req "name" string ~title:"Name"
                 ~description:
                   "Filename/URI-compliant name of the specification.")
              (req "title" string ~title:"Title"
                 ~description:
                   "Single sentence title describing the specification")
              (dft "status" status `Work_in_progress ~title:"Current-status"
                 ~description:"Current version or work-in-progress.")
              (dft "authors" (list string) [] ~title:"Authors"
                 ~description:
                   "The list of authors of the specification, preferably in \
                    `Print Name <email@example.com>` form.")
              (opt "introduction" prose
                 ~title:
                   "Introductory blob; actual extensive description of the \
                    specification, who/what it is for.")
              (dft "contracts" (list contract) [] ~title:"Contract interfaces"
                 ~description:
                   "The list of contract interface that the specification \
                    defines."))) in
    With_version.(encoding ~name (first_version v0))

  let to_json t =
    let open Data_encoding in
    Json.construct encoding t

  let of_json j =
    let open Data_encoding in
    try Json.destruct encoding j
    with e ->
      Fmt.failwith "Parsing Scenario: %a"
        (Json_encoding.print_error ~print_unknown:Fmt.exn)
        e

  let json_schema =
    let open Data_encoding in
    let schema = Json.schema encoding in
    Json.construct json_schema schema
end
