Micodef: Michelson Contract Specification
=========================================

This a **work-in-progress.**

The aim is to provide a way of specifying Michelson smart-contract interfaces
for interoperability purposes.

This repository provides:

* The specification of the specification format (using the `data-encoding`
  library).
* A command-line tool, `micodef`, to manipulate the specification format:
     * Generation of the JSON Schema version of the specification.
     * A few examples of specifications output as JSON.
     * Conversion from/to YAML.
     * Conversion to HTML.

Build
-----

Some dependencies can be obtained with:

    make deps
    make vendors

Then build with:

    make

Docker Image
------------

You can use `micodef` through a “thin” docker image:

```
 $ docker run --rm \
          registry.gitlab.com/smondet/micodef:02d8108f-run \
          micodef examples --desc --all
Examples:
  * `empty-spec`: The minimal specification.
  * `one-entry-point`: Example specification with one single entry-point.
  * `two-entry-point`: Example specification with two entry-points.
  * `full-example`: Fuller specification with many fields filled.
```

