# Examples


## Artwork Licensing

This example uses YAML as the source for the specification. It defines some
contracts to which would be used *as and by* a hypothetical/work-in-progress/toy
copyright registration service.

<!--artwork-licensing-->


## TZIP-7 Derivate

This specification also uses the YAML as input. It defines something similar to
[FA1.2/TZIP-7](https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-7/tzip-7.md).

<!--fa-one-dot-something-->

