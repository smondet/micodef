.PHONY: build
build:
	dune build src/app/main.exe && ln -sf _build/default/src/app/main.exe micodef

.PHONY: clean
clean:
	dune clean && rm micodef

.PHONY: vendors
vendors:
	sh src/scripts/ensure-vendors.sh

.PHONY: deps
deps:
	opam install -y dune.1.11.3 depext
	opam depext -i dune.1.11.3 omd base cmdliner ezjsonm \
             yaml fmt ocplib-endian ppx_compare \
             ppx_sexp_conv tyxml uri zarith ocamlformat.0.10

.PHONY: fmt
fmt:
	find src \( -name '*.ml' -o -name '*.mli' \) -exec ocamlformat -i --enable-outside {} \;

.PHONY: all
all: build
